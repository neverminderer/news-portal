'use strict';
module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        models.User.hasMany(models.News, {foreignkey: 'id'});
        models.User.hasMany(models.Comment, {foreignkey: 'id'});
      }
    }
  });
  return User;
};
/*var DataTypes = require('sequelize');
var mysql = require("mysql");
var sequelize = new DataTypes('mysql://root:content@cloud.bs.ua:3310/Masha');
var News      = require('./news');

var User = sequelize.define('user_test', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    unique: true,
    autoIncrement: true
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false
  },
  admin: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: false
  }
}, {
  timestamps: true
},{
  classMethods: {
    associate: function() {
      User.hasMany(News, {foreignkey: 'id'});
      User.hasMany(Comment, {foreignkey: 'id'});
    }
  }
});

// make this available to our Node applications
module.exports = User;*/
