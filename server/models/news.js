'use strict';
module.exports = function(sequelize, DataTypes) {
  var News = sequelize.define('News', {
    text: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    image: DataTypes.STRING.BINARY
  }, {
    classMethods: {
      associate: function(models) {
        models.News.belongsTo(models.User, {foreignkey: 'id'});
        models.News.hasMany(models.Comment, {foreignkey: 'user_id'});
      }
    }
  });
  return News;
};
/*var DataTypes = require('sequelize');
var mysql     = require("mysql");
var sequelize = new DataTypes('mysql://root:content@cloud.bs.ua:3310/Masha');
var User      = require('./users');

var News = sequelize.define('news_test', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      unique: true,
      autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    text: {
        type: DataTypes.STRING,
        allowNull: false
    },
    image: {
        type: DataTypes.STRING.BINARY,
        allowNull: false
    },
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
  classMethods: {
    associate: function() {
      News.belongsTo(User, {foreignkey: 'id'});
      News.hasMany(Comment, {foreignkey: 'id'});
    }
  }
},{
  timestamps: true
});

module.exports = News;
// make this available to our Node applications*/
