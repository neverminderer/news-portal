'use strict';
module.exports = function(sequelize, DataTypes) {
  var Comment = sequelize.define('Comment', {
    text: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
    news_id: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        models.Comment.belongsTo(models.News, {foreignkey: 'news_id'});
        models.Comment.belongsTo(models.User, {foreignkey: 'user_id'});
      }
    }
  });
  return Comment;
};

/*var DataTypes = require('sequelize');
var mysql     = require("mysql");
var sequelize = new DataTypes('mysql://root:content@cloud.bs.ua:3310/Masha');
var News      = require('./news');
var User      = require('./user');

var Comment = sequelize.define('comment_test', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      unique: true,
      autoIncrement: true
    },
    text: {
        type: DataTypes.STRING,
        allowNull: false
    },
    news_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
  classMethods: {
    associate: function() {
      Comment.belongsTo(News, {foreignkey: 'news_id'});
      Comment.belongsTo(User, {foreignkey: 'user_id'});
    }
  }
},{
  timestamps: true
});
// make this available to our Node applications
module.exports = Comment;*/
