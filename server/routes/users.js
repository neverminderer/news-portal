var express   = require('express');
var router    = express.Router();
var Verify    = require('./verify');
var crypto    = require('crypto');
var models = require('../models/index');

router.get('/', function(req, res) {
  models.User.sync().then(function () {
    return models.User.findAll({});
  })
  .then(function(users) {
    return res.json(users);
  }).catch(function(e){
     return res.status(500).json({err: e.message});
  })
});


router.post('/register', function(req, res) {

  var shasum = crypto.createHash('sha1');
  shasum.update(req.body.password);
  req.body.password = shasum.digest('hex');

  models.User.sync().then(function () {
   return  models.User.findOrCreate({where: {username: req.body.username,
                                       password: req.body.password}})
                .spread(function(created) {

                    if ( !created ) {
                      return res.status(500).json({err: 'User with this name is already registred!'});
                    } else {
                      return res.status(200).json({status: 'Registration Successful!'});
                }

    }).catch(function(e){
       return res.status(500).json({err: e.message});
    })
  });
});
router.post('/login', function(req, res) {

    var shasum = crypto.createHash('sha1');
    shasum.update(req.body.password);
    req.body.password = shasum.digest('hex');

    models.User
      .findOne({where: {username: req.body.username, password: req.body.password}})
      .then(function(user){

      if (!user) {
        return res.status(401).json({
          err: 'Could not log in user'
        });
      }

      var token = Verify.getToken({
        id: user.get('id'),
        username: user.get('username'),
        admin: user.get('admin')
      });

      res.status(200).json({
        status: 'Login successful!',
        success: true,
        token: token
      });
    }).catch(function(e){
       return res.status(500).json({err: e.message});
    })
});

router.get('/logout', function(req, res) {
  req.logout();
  res.status(200).json({
    status: 'Bye!'
  });
});

router.route('/:id')
.all(Verify.verifyAdmin)

.put(function(req, res) {
  models.User.update({
    username: req.body.username,
    admin: req.body.admin
  },{where: {id: req.params.id}
  }).then(function(user) {
    res.send(user);
  }).catch(function(e){
     return res.status(500).json({err: e.message});
  })
})

.delete(function(req, res) {
  models.User.destroy({
    where: {
      id: req.params.id
    }
  }).then(function(user) {
    res.json(user)
  }).catch(function(e){
     return res.status(500).json({err: e.message});
  })
});

module.exports = router;
