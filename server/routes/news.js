var express = require('express');
var bodyParser = require('body-parser');
var models = require('../models/index');
var Verify    = require('./verify');
var newsRouter = express.Router();

newsRouter.use(bodyParser.json());

newsRouter.route('/')

.get(function(req, res) {
  models.News.findAll({})
  .then(function(news) {
    res.json(news);
  }).catch(function(e){
     return res.status(500).json({err: e.message});
  })
})

.post(Verify.verifyOrdinaryUser, function(req, res) {
  models.News.sync().then(function () {
    return models.News.create({
      name: req.body.name,
      image: req.body.image,
      text: req.body.text,
      user_id: req.decoded.id
    })
  })
  .then(function(news) {
      res.json(news);
  })
  .catch(function(e){
     return res.status(500).json({err: e.message});
  })
});

newsRouter.route('/user')

.get(Verify.verifyOrdinaryUser, function(req, res) {
  models.News.sync().then(function () {
    return News.findAll({
      where:
      {
        user_id: req.decoded.id
      }
    });
  })
  .then(function(news) {
    res.json(news);
  }).catch(function(e){
     return res.status(500).json({err: e.message});
  })
})

newsRouter.route('/:id')

.get(function(req, res) {
  models.News.sync().then(function () {
    return models.News.findOne({
      where: {
        id: req.params.id
      }
    })
  }).then(function (news) {
    res.json(news);
  }).catch(function(e){
     return res.status(500).json({err: e.message});
  })
})

.put(Verify.verifyOrdinaryUser, function(req, res) {
  models.News.findOne({
    where: {
      id: req.params.id
    }
  }).then(function(news) {

    if (req.decoded.id === news.get('user_id') || !req.decoded.admin) {
      return res.status(401).json({err: 'You are not authenticated!'});
    }

    if(news){
      news.updateAttributes({
        name: req.body.name,
        image: req.body.image,
        text: req.body.text
      }).then(function(news) {
        res.send(news);
      })
    }
  }).catch(function(e){
     return res.status(500).json({err: e.message});
  })
})

.delete(Verify.verifyOrdinaryUser, function(req, res) {
  models.News.findOne({
    where: {
      id: req.params.id
    }
  }).then(function(news) {

    if (req.decoded.id === news.get('user_id') || req.decoded.admin) {
      return comment;
    }

    return res.status(401).json({err: 'You are not authenticated!'});

  }).then(function(news) {
    return models.News.destroy({
      where: {
        id: req.params.id
      }
    })
  }).catch(function(e){
     return res.status(500).json({err: e.message});
  })
});

newsRouter.route('/:newsId/comment')

.get(Verify.verifyOrdinaryUser, function(req, res) {
  models.Comment.sync().then(function () {
    return models.Comment.findAll({where:{news_id:req.params.newsId}});
  })
  .then(function(comment) {
    res.json(comment);
  }).catch(function(e){
     return res.status(500).json({err: e.message});
  })
})

.post(Verify.verifyOrdinaryUser, function(req, res) {
  models.Comment.sync().then(function () {
    return models.Comment.create({
      text: req.body.text,
      user_id: req.decoded.id,
      news_id: req.params.newsId
    })
  }).then(function(comment) {
      res.json(comment);
  })
  .catch(function(e){
     return res.status(500).json({err: e.message});
  })
});

newsRouter.route('/:id/comment/:commentId')

.delete(Verify.verifyOrdinaryUser, function(req, res) {
  models.Comment.findOne({
    where: {
      id: req.params.commentId
    }
  }).then(function(comment) {

    if (req.decoded.id === comment.get('user_id') || req.decoded.admin) {
      return comment;
    }

    return res.status(401).json({err: 'You are not authenticated!'});

  }).then(function(comment) {
    return models.Comment.destroy({
      where: {
        id: req.params.commentId
      }
    })
  }).catch(function(e){
     return res.status(500).json({err: e.message});
  })
});
module.exports = newsRouter;
