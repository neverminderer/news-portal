'use strict';

    angular
        .module('app.news')
        .factory('NewsService', Service)
        .constant("baseURL", "https://localhost:3443/");

    function Service($http, $q, baseURL) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.GetAllComments = GetAllComments;
        service.CreateComments = CreateComments;

        return service;

        function GetAll() {
            return $http.get(baseURL+'news')
            .then(handleSuccess, handleError);
        }

        function GetById(_id) {
            return $http.get(baseURL+'news/' + _id)
            .then(handleSuccess, handleError);
        }

        function Create(news) {
            return $http.post(baseURL+'news', news)
            .then(handleSuccess, handleError);
        }

        function Update(news) {
            return $http.put(baseURL+'news/' + news._id, news)
            .then(handleSuccess, handleError);
        }

        function Delete(_id) {
            return $http.delete(baseURL+'news/' + _id)
            .then(handleSuccess, handleError);
        }

        function GetAllComments(_id) {
            return $http.get(baseURL+'news/' + _id + '/comment')
            .then(handleSuccess, handleError);
        }

        function CreateComments(news, comment) {
            return $http.post(baseURL+'news/' + news._id + '/comment', comment)
            .then(handleSuccess, handleError);
        }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(res) {
            return $q.reject(res.data);
        }
    }
