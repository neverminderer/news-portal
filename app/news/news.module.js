'use strict';

    angular
        .module('app.news', [])
        .constant('_', window._)
        .controller('PagerController', ["NewsService", "PagerService", function (NewsService, PagerService) {
              var vm = this;
              vm.newsDb = [];
              NewsService.GetAll().then( function (news) {

                  for (var key in news) {
                    vm.newsDb.push(news[key]);
                  }

              vm.pager = {};
              vm.setPage = setPage;

              initController();

              function initController() {
                  // initialize to page 1
                  vm.setPage(1);
              }

              function setPage(page) {
                  if (page < 1 || page > vm.pager.totalPages) {
                      return;
                  }

                  // get pager object from service
                  vm.pager = PagerService.GetPager(vm.newsDb.length, page);

                  // get current page of items
                  vm.items = vm.newsDb.slice( vm.pager.startIndex, vm.pager.endIndex);
              }
            });

        }]);
