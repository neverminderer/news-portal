'use strict';

angular
    .module('app.newspage', ['app.news', 'app.header'])
    .controller('NewsController', function () {
    var vm = this;
        vm.news = {
            _id: 'djkadwkdwdw34jij2343n4k2j3n4',
            header: 'Очень классное название',
            img: 'images/img.jpg',
            text: `Полузащитник «Ромы» Миралем Пьянич хотел бы продолжить свою карьеру в составе «Барселоны». В каталонском клубе также серьезно присматриваются к футболисту сборной Боснии и Герцеговины. Конкуренцию в борьбе за форварда им составят «Ювентус», «Бавария» и «ПСЖ». Ранее сообщалось, что туринский клуб уже сделал предложение «Роме» на сумму в 38 миллионов евро.`,
            author: 'Vasia',
            createdAt: '2015.05.01'
        }
    })
    .controller('CommentsController', ['AuthFactory', '$rootScope', 'ngDialog', 'NewsService',
    function (AuthFactory, $rootScope, ngDialog, NewsService) {

    var vm = this;
    vm.comments = NewsService.GetAllComments();
    vm.loggedIn = false;


    if(AuthFactory.isAuthenticated()) {
        vm.loggedIn = true;
        vm.username = AuthFactory.getUsername();
    };

    $rootScope.$on('login:Successful', function () {
        vm.loggedIn = AuthFactory.isAuthenticated();
        vm.username = AuthFactory.getUsername();
    });

    $rootScope.$on('registration:Successful', function () {
        vm.loggedIn = AuthFactory.isAuthenticated();
        vm.username = AuthFactory.getUsername();
    });

    vm.submitComment = function () {
          vm.mycomment.date = new Date().toISOString();
          NewsService.CreateComments(mycomment)
    }


    vm.comments = [
            {
                text: 'Using HTML5 mode means that we can have clean URLs for our application routes.',
                author: 'Vasia',
                createdAt: '2015.01.05'
            },
            {
                text: 'Using HTML5 mode means that we can have clean URLs for our application routes.',
                author: 'Vasia',
                createdAt: '2015.01.05'
            },
            {
                text: 'Using HTML5 mode means that we can have clean URLs for our application routes.',
                author: 'Vasia',
                createdAt: '2015.01.05'
            },
            {
                text: 'Using HTML5 mode means that we can have clean URLs for our application routes.',
                author: 'Vasia',
                createdAt: '2015.01.05'
            },
            {
                text: 'Using HTML5 mode means that we can have clean URLs for our application routes.',
                author: 'Vasia',
                createdAt: '2015.01.05'
            },
            {
                text: 'Using HTML5 mode means that we can have clean URLs for our application routes.',
                author: 'Vasia',
                createdAt: '2015.01.05'
            }
        ];
    }])
