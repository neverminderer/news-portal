'use strict';

angular.module('app', ['ui.router','ngResource','app.header', 'app.account', 'app.news', 'app.newspage'])
.config(function($stateProvider, $urlRouterProvider) {

        $stateProvider

            // route for the home page
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'header/header.html',
                    },
                    'content': {
                        templateUrl : 'news/news.html',
                    },
                }

            })

            .state('app.newspage', {
                url:':id',
                views: {
                    'content@': {
                        templateUrl : 'newspage/news.html',
                    },
                }

            })

            $urlRouterProvider.otherwise('/');
    })
