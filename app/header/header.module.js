'use strict';

angular
    .module('app.header', ['ngDialog', 'app.account'])
    .controller ('HeaderController', ['AuthFactory', '$rootScope', 'ngDialog',
                 function (AuthFactory, $rootScope, ngDialog) {
        var vm = this;

        vm.loggedIn = false;
        vm.username = '';

        if(AuthFactory.isAuthenticated()) {
            vm.loggedIn = true;
            vm.username = AuthFactory.getUsername();
        };

        vm.openLogin = function (vm) {
              ngDialog.open({ template: '/account/registr-modal.html', scope: vm,
              className: 'ngdialog-theme-default', controller:"LoginController",
              controllerAs:"ctrl" });
        };

        $rootScope.$on('login:Successful', function () {
            vm.loggedIn = AuthFactory.isAuthenticated();
            vm.username = AuthFactory.getUsername();
        });

        $rootScope.$on('registration:Successful', function () {
            vm.loggedIn = AuthFactory.isAuthenticated();
            vm.username = AuthFactory.getUsername();
        });

    }])
