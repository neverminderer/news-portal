'use strict';

angular
    .module('app.account', ['app.header', 'ngDialog'])

    .controller('LoginController', ['AuthFactory', '$localStorage', 'ngDialog',
                 function (AuthFactory, $localStorage, ngDialog) {
    var vm = this;
    vm.loginData = $localStorage.getObject('userinfo','{}');

    vm.doLogin = function() {
        AuthFactory.login(vm.loginData, vm.rememberMe);
        ngDialog.close();
    };


    vm.doRegister = function() {
        console.log('Doing registration', vm.registration);
        AuthFactory.register(vm.registration);
        ngDialog.close();
    };

    }])
