'use strict';

angular.
    module('app.account')
    .factory('$localStorage', ['$window', function ($window) {
        return {
            store: function (key, value) {
                $window.localStorage[key] = value;
            },
            get: function (key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            remove: function (key) {
                $window.localStorage.removeItem(key);
            },
            storeObject: function (key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function (key, defaultValue) {
                return JSON.parse($window.localStorage[key] || defaultValue);
            }
        }
    }])

    .factory('AuthFactory', ['$resource', '$http', '$localStorage', '$rootScope', '$window', 'ngDialog',
                         function($resource, $http, $localStorage, $rootScope, $window, ngDialog){

    var authFac = {};
    var TOKEN_KEY = 'Token';
    var isAuthenticated = false;
    var username = '';
    var authToken = undefined;


      function loadUserCredentials() {
        var credentials = $localStorage.getObject(TOKEN_KEY,'{}');
        if (credentials.username != undefined) {
          useCredentials(credentials);
        }
      }

      function storeUserCredentials(credentials) {
        $localStorage.storeObject(TOKEN_KEY, credentials);
        useCredentials(credentials);
      }

      function useCredentials(credentials) {
        isAuthenticated = true;
        username = credentials.username;
        authToken = credentials.token;

        // Set the token as header for your requests!
        $http.defaults.headers.common['x-access-token'] = authToken;
      }

      authFac.login = function(loginData, remember) {
          $resource("https://localhost:3443/users/login")
          .save(loginData,
             function(response) {

                if ( remember === true ) {
                  storeUserCredentials({username:loginData.username, token: response.token});
                }

                $rootScope.$broadcast('login:Successful');
             },
             function(response){
                isAuthenticated = false;
                console.log(response.data)

              }

          );

    };

    authFac.register = function (registerData) {
                             $http.post("https://localhost:3443/users/register", registerData, {})
                             .then(registerData, handleError);
                           };

   /* authFac.register = function(registerData) {
        $resource("https://localhost:3443/users/register")
        .save(registerData,
           function(response) {

              authFac.login({username:registerData.username, password:registerData.password});
              $rootScope.$broadcast('registration:Successful');
           },
           function(response){


           }

        );
    };*/

    authFac.isAuthenticated = function() {
        return isAuthenticated;
    };

    authFac.getUsername = function() {
        return username;
    };

    loadUserCredentials();

    return authFac;

}])
;
